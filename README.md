# automatic-tests

## General info
Project made to show my skills in python automatic testing. Currently there is one project which i have tests for - [simple-rest-api-server](https://gitlab.com/dimiolek-code-examples/simple-rest-api-server)
	
## Technologies
Project is created with:
* python: 3.8.8
* pytest: 6.2.4
* docker: 20.10.7
* docker-compose: 1.29.2
	


## TODO

* add more tests
* add selenium tests (ui)
* add performance tests (locust)