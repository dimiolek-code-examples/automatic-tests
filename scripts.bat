@ECHO OFF

if "%1" == "install-venv" (
    "python.exe" -m pip install virtualenv
    "python.exe" -m virtualenv venv
) else if "%1" == "install" (
    "./venv/Scripts/pip.exe" install -r requirements.txt
) else if "%1" == "update-pip" (
    "./venv/Scripts/pip.exe" freeze > requirements.txt
) else if "%1" == "test" (
    "./venv/Scripts/python.exe" -m pytest -vv -r A -durations=0 tests/projects/simple_rest_api_server