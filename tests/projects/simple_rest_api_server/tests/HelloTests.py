from unittest import TestCase

from tests.projects.simple_rest_api_server.src.fixtures import SimpleRestApiServerFixture
from tests.projects.simple_rest_api_server.src.testers import SimpleRestApiServerTester


class FindAccountsTests(TestCase):
    def setUp(self) -> None:
        self.tester = SimpleRestApiServerTester()

    def test_check_correct_name(self):
        # given
        some_name = SimpleRestApiServerFixture.some_name()
        # when
        self.tester.get_hello(some_name)
        # then
        self.tester.see_correct_name(some_name)

    def test_check_post_method(self):
        # given
        some_name = SimpleRestApiServerFixture.some_name()
        # when
        self.tester.post_hello(some_name)
        # then
        self.tester.see_status_code(405)
