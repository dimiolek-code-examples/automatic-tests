import os

from lib.helpers.ConfigLoader import ConfigLoader

path = os.path.dirname(__file__)

SIMPLE_REST_API_SERVER_CONFIG = ConfigLoader.get_config(path)['simple-rest-api-server']
