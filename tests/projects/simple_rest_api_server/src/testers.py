from tests.projects.simple_rest_api_server import SIMPLE_REST_API_SERVER_CONFIG
from tests.src.testers.JsonApiTester import JsonApiTester


class SimpleRestApiServerTester(JsonApiTester):
    def __init__(self, config=SIMPLE_REST_API_SERVER_CONFIG, *args, **kwargs):
        super().__init__(config=config, *args, **kwargs)

    def get_hello(self, name):
        return self.send_get(f'hello/{name}')

    def post_hello(self, name):
        return self.send_post(f'hello/{name}')

    # ASSERTS

    def see_correct_name(self, name):
        self.see_text(f'Hello {name}')
