from typing import Dict


class Tester:
    def __init__(self, config: Dict = None):
        self.config = config

    def do(self, use_case):
        return use_case(self)
