from typing import Dict
from urllib.parse import urljoin

import requests

from lib.helpers.DebugHelper import DebugHelper
from tests.src.testers.Tester import Tester


class HttpTester(Tester):
    def __init__(self, session=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        http_config = self.config['http']
        default_headers = http_config['default_headers']
        self._url = http_config['url']
        self._headers = {}
        self.set_headers(**default_headers)
        self._last_response: requests.Response
        if not session:
            session = requests.session()
        self._session = session

    def grab_last_response(self) -> requests.Response:
        return self._last_response

    def _request(self, method: str, path: str, **kwargs) -> requests.Response:
        headers = self._headers.copy()
        headers.update(kwargs.get('headers', {}))
        kwargs['headers'] = headers
        result = self._session.request(
            method=method,
            url=urljoin(self._url, path),
            **kwargs)
        self._last_response = result
        return result

    def debug_response(self):
        print(DebugHelper.get_printable_response(self._last_response))

    def send_get(self, path: str, params: Dict = None, **kwargs_params):
        params = dict(**params or {}, **kwargs_params or {})
        return self._request('GET', path, params=params).content.decode()

    def send_post(self, path: str, json: (Dict, str, list) = None, data: (Dict, str, list) = None, params: Dict = None,
                  **kwargs):
        return self._request('POST', path, json=json, data=data, params=params, **kwargs).content.decode()

    def set_headers(self, **headers):
        self._headers.update(headers)

    def remove_header(self, name: str):
        del self._headers[name]

    def grab_text(self) -> str:
        return self._last_response.content.decode()

    def see_text(self, text: str):
        assert self._last_response.content.decode().strip().replace("\"", "") \
               == text, 'Unexpected response text content. Expected text "%s".\n%s' % (
            text, DebugHelper.get_printable_response(self._last_response))

    def see_text_contains(self, text: str):
        assert \
            self._last_response.content.decode().find(text), \
            'Not found expected text in response. Expected text "%s".\n%s' % (
                text,
                DebugHelper.get_printable_response(self._last_response)
            )

    def see_status_code(self, status_code: int):
        assert \
            self._last_response.status_code == status_code, \
            'Unexpected code response. Expected code "%d".\n%s' % (
                status_code,
                DebugHelper.get_printable_response(self._last_response)
            )
