from typing import Dict, List, Union

from lib.helpers.AssertHelper import AssertHelper
from lib.helpers.DebugHelper import DebugHelper
from tests.src.testers.HttpTester import HttpTester


class JsonApiTester(HttpTester):
    def _grab_last_json(self):
        last_response = self.grab_last_response()
        if last_response.content:
            return last_response.json()
        else:
            return None

    def send_get(self, path: str, params: Dict = None, **kwargs_params):
        HttpTester.send_get(self, path, params, **kwargs_params)
        return self._grab_last_json()

    def send_post(self, path: str, json: (Dict, str, list) = None, data: (Dict, str, list) = None, params: Dict = None,
                  **kwargs):
        HttpTester.send_post(self, path, json, data, params, **kwargs)
        return self._grab_last_json()

    def grab_json(self) -> Union[Dict, List]:
        return self._grab_last_json()

    def see_json(self, json_data: Union[Dict, List]):
        assert \
            json_data == self._grab_last_json(), \
            'Unexpected JSON response. Expected "%s".\n%s' % (
                json_data,
                DebugHelper.get_printable_response(self._last_response)
            )

    def see_json_contains(self, json_data: Union[Dict, List]):
        assert \
            AssertHelper.is_contains(json_data, self._grab_last_json()) is True, \
            'Not found expected JSON in response. Expected "%s".\n%s' % (
                json_data,
                DebugHelper.get_printable_response(self._last_response)
            )

    def see_json_elements_number(self, number_of_elements: int):
        last_response_elements_number = len(self.grab_json())
        assert \
            last_response_elements_number == number_of_elements, \
            'Unexpected number of elements in JSON response. Expected %d but found %d.\n%s' % (
                number_of_elements,
                last_response_elements_number,
                DebugHelper.get_printable_response(self._last_response)
            )

    def see_any_json_element(self):
        last_response_elements_number = len(self.grab_json())
        assert \
            last_response_elements_number > 0, \
            'Do not see any json elements in response \n%s' % (
                DebugHelper.get_printable_response(self._last_response)
            )
