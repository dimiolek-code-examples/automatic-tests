import difflib
from copy import copy


class AssertHelper:
    @classmethod
    def is_contains(cls, wanted, searched):

        searched_type = type(searched)
        wanted_type = type(wanted)
        if wanted_type == type:
            return issubclass(searched_type, wanted)
        elif searched_type != type(wanted):
            return False
        elif searched_type == list:
            searched = copy(searched)
            for wanted_sub in wanted:
                found_sub = False
                for index, searched_sub in enumerate(searched):
                    if cls.is_contains(wanted_sub, searched_sub):
                        found_sub = True
                        searched.pop(index)
                        break
                if not found_sub:
                    return False
            return True
        elif searched_type == dict:
            for wanted_key, wanted_value in wanted.items():
                searched_value = searched.get(wanted_key)
                is_key_occurs = wanted_key in searched
                if not is_key_occurs or cls.is_contains(wanted_value, searched_value) is False:
                    return False
            return True
        elif wanted == searched:
            return True
        else:
            return False

    @classmethod
    def assert_files(cls, file_path1: str, file_path2: str, encoding='windows-1250'):
        file1 = open(file_path1, 'r', encoding=encoding)
        file2 = open(file_path2, 'r', encoding=encoding)
        diff = difflib.unified_diff(
            file1.readlines(),
            file2.readlines())
        file1.close()
        file2.close()
        error = False
        for line in diff:
            error = True
            print(line.strip('\n\r'))
        if error:
            raise Exception('The files compared are different')
