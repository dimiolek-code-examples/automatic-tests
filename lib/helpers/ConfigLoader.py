import os
import yaml
from deepmerge import always_merger

DIST_CONFIG_FILENAME = 'config.dist.yml'
USER_CONFIG_FILENAME = 'config.yml'


class ConfigLoader:
    @classmethod
    def get_config(cls, dir_path: str) -> dict:
        dist_config_path = os.path.join(dir_path, DIST_CONFIG_FILENAME)
        user_config_path = os.path.join(dir_path, USER_CONFIG_FILENAME)
        dist_config = None
        user_config = None

        if os.path.exists(dist_config_path):
            with open(dist_config_path, 'r') as file:
                dist_config = yaml.safe_load(file)

        if os.path.exists(user_config_path):
            with open(user_config_path, 'r') as file:
                user_config = yaml.safe_load(file)

        if user_config or dist_config:
            return always_merger.merge(
                dist_config or {},
                user_config or {}
            )
        else:
            raise Exception('not found any of config files %s'
                            % ([dist_config_path, user_config_path]))
