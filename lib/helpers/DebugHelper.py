from pprint import pformat
import requests


class DebugHelper:
    @classmethod
    def get_printable_response(cls, response: requests.models.Response) -> str:
        try:
            response_content = response.json()
        except ValueError:
            response_content = response.content.decode()

        return pformat(dict(
            request_method=response.request.method,
            request_url=response.request.url,
            request_body=response.request.body,
            response_code=response.status_code,
            response_content=response_content
        ))
