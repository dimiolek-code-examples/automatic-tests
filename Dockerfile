FROM python:3.8-slim-buster

COPY requirements.txt .
RUN pip install -r requirements.txt
COPY . .

ENTRYPOINT ["python"]
CMD ["-c", "print('specify test command')"]
